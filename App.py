from flask import Flask, make_response, request, render_template
from form import LoginForm
app = Flask(__name__)


@app.route('/')
def Index():

    return  render_template('index.html')

@app.route('/about')
def About():
    context = {
        'text':'Test text for about',
        'name': 'Nick'
    }
    return render_template('about.html', data=context)

@app.route('/login', methods = ["GET", "POST"])
def Login():
    form = LoginForm()
    return render_template('login.html', title = 'Login', form = form)

@app.errorhandler(404)
def page_not_found(e):
    context = {
        'text': 'Page Not Found',
        'name' : 'Blame Nick'
    }
    return render_template('404.html', data=context)


if __name__ == "__main__":
    app.run(debug=True)